task1:
steps-
1) go to vpc dashboard
2) click on launch vpc wizard
3) enable auto assign public ip to public subnet

task2:
steps-
1) create an ec2 instance in public subnet and choose windows ami
2) create an ec2 instance in private subnet and choose linux ami
3) now ssh to windows instance and ping linux instance

task3:
1) for windows instance create security group and assign port to it
2) aws ec2 create-security-group --group-name **** --description "****" --vpc-id vpc-****
3) aws ec2 authorize-security-group-ingress --group-id **** --ip-permissions IpProtocol=tcp,FromPort=3389,ToPort=3389,IpRanges=[{CidrIp=0.0.0.0/0}] IpProtocol=icmp,FromPort=-1,ToPort=-1,IpRanges=[{CidrIp=0.0.0.0/0}]
4) now create a windows instance in public subnet
5) aws ec2 run-instances --image-id ami-xxxx --count 1 --instance-type t2.micro --key-name #### --security-group-ids sg-xxxx --subnet-id subnet-xxxx
6) create another security group for linux instance
7) create linux instance
8) ssh to windows and ping linux