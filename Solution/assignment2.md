task1:
steps-
1) create a vpc
2) create 1 public,1 private and 1 protected subnet in one availability zone
3) create 1 public,1 private and 1 protected subnet in another availability zone
4) create 1 IGW and 2 NAT GATEWAYS
5) create public_route_table and associate both the public subnets and route it to IGW
6) create private_route_table_1 and associate 1 private subnet and route it to NAT Gateway
7) craete private_route_table_2 and associate another private subnet and route it to another NAT Gateway
8) create protected_route_table and associate both the protected subnets

task2:
steps-
1) create private instance in private subnet of one availability zone
2) create another private instance in private subnet of another availability zone
3) ssh to public instance of first availability zone and then ssh to private instance through it
4) install lamp setup and serve a page "hi i am server 1"
5) crete private instance in private subnet of second availability zone 
6) ssh to public instance of second availability zone and then ssh to to private instance throught it
7) install lamp setup and serve a page "hi i am server2"

task3:
steps-
1) go to ec2 and click on classic load balancer then assign the name "publicloadbalancer" and choose the vpc and subnets ie private subnets 
2) configure security group then pick both the private instances
3) then add tag and create
