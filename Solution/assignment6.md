task1:
steps-
1) find a domain name from freenom "waquaropstree.com"
2) go to s3 and create bucket "waquaropstree.com", bucket name must be same as a domain name
3) upload index.html file inside the bucket and grant it public read access
4) then go to properties and configure static website hosting where we find endpoint
5) go to route 53 and create hosted zone
6) in domain name "waquaropstree.com" and type "public hosted zone" and then click on create record set where we choose s3 endpoint from alias
7) then we update freenom webserver records with route 53 name server records