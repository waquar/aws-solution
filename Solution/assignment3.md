task1:
steps-
1) create t2.micro ec2 instance and then ssh to it
2) go to sudoers file and rename it
   cd /etc /sudoers
   mv sudoers sudoers.bkp
3) now go to ec2 instance and stop it and then detach the ebs volume
4) create another t2.micro ec2 instance with same config and attach that volume to it
5) ssh to the instance and find the root partition and mount it to temporary location
   lsblk
   mkdir /newmount
   mount /dev/xvdf1 /newmount
6) fix its chown ownership
   chown root:root ./etc/sudoers.bkp
7) now go to console detach the voluma and again attch it to the instance